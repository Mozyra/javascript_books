let books = document.querySelectorAll(".book");
console.log(books);
books[0].before(books[1]);
books[0].after(books[4]);
books[4].after(books[3]);
books[3].after(books[5]);
let backgroundImage = (document.body.style.backgroundImage =
  "URL(image/you-dont-know-js.jpg)");

let reklama = document.querySelector(".adv");
reklama.remove();

let title = document.querySelectorAll("h2");
title[2].textContent = "Книга 3. This и Прототипы Объектов";
title[2].style.color = "darkkhaki";

let bookTwo = books[0];
let list = bookTwo.querySelectorAll("li");

list[3].after(list[6]);
list[6].after(list[8]);
list[9].after(list[2]);

let bookFive = books[5];
let listFive = bookFive.querySelectorAll("li");
listFive[1].after(listFive[9]);
listFive[4].after(listFive[2]);
listFive[7].after(listFive[5]);

let sixBook = books[2];
let newUl = sixBook.querySelector("ul");
let li = document.createElement("li");
li.innerHTML = "Глава 8: За пределами ES6";
newUl.appendChild(li);
let listSix = sixBook.querySelectorAll("li");
listSix[10].after(listSix[9]);
console.log(listSix);
